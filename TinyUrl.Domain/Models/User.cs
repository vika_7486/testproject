﻿namespace TinyUrl.Domain.Models
{
    public class User : Entity
    {
        public string Name { get; set; }
        public string Password { get; set; }

        public User() : base()
        {
        }

        public User(string name, string password)
        {
            Name = name;
            Password = password;
        }
    }
}
