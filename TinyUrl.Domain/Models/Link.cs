﻿using System;

namespace TinyUrl.Domain.Models
{
    public class Link : Entity
    {
        public string OriginalUrl { get; set; }
        public string UniqueCode { get; set; }
        public DateTime CreationDateTime { get; set; }
        public short ReferenceCount { get; set; }
        public Guid UserId { get; set; }

        public Link() : base()
        {
        }

        public Link(string originalUrl, string uniqueCode, Guid userId)
        {
            OriginalUrl = originalUrl;
            UniqueCode = uniqueCode;
            UserId = userId;
        }
    }
}
