﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TinyUrl.Domain.Models;

namespace TinyUrl.Domain.Repositories
{
    public interface ILinkRepository
    {
        Task<Link> Get(string code);
        Task<Link> Get(Guid userId, string originalUrl);
        Task<IEnumerable<Link>> Get(Guid userId);
        Task Create(Link link);
        Task UpdateCount(Link link);
    }

    public class LinkRepository : ILinkRepository
    {
        private readonly DapperDbContext _context;

        public LinkRepository(DapperDbContext dapperDbContext)
        {
            _context = dapperDbContext;
        }

        public async Task<Link> Get(string code)
        {
            using var db = _context;
            var sqlQuery = "SELECT * FROM tiny_url.link WHERE unique_code = @Code";
            return await db.Open.QueryFirstOrDefaultAsync<Link>(sqlQuery, new { code });
        }

        public async Task<Link> Get(Guid userId, string originalUrl)
        {
            using var db = _context;
            var sqlQuery = "SELECT * FROM tiny_url.link WHERE user_id = @UserId AND original_url = @OriginalUrl";
            return await db.Open.QueryFirstOrDefaultAsync<Link>(sqlQuery, new { UserId = userId, OriginalUrl = originalUrl });
        }

        public async Task<IEnumerable<Link>> Get(Guid userId)
        {
            using var db = _context;
            var sqlQuery = "SELECT * FROM tiny_url.link WHERE user_id = @UserId";
            return await db.Open.QueryAsync<Link>(sqlQuery, new { UserId = userId });
        }

        public async Task Create(Link link)
        {
            using var db = _context;
            var sqlQuery = "INSERT INTO tiny_url.link(id, original_url, unique_code, user_id) VALUES(@Id, @OriginalUrl, @Code, @UserId)";
            await db.Open.ExecuteAsync(sqlQuery, new { link.Id, link.OriginalUrl, Code = link.UniqueCode, link.UserId });
        }

        public async Task UpdateCount(Link link)
        {
            using var db = _context;
            var sqlQuery = "WITH updated AS (UPDATE tiny_url.link SET reference_count = reference_count + 1 WHERE id = @Id RETURNING reference_count) SELECT * FROM updated;";
            await db.Open.ExecuteAsync(sqlQuery, new { link.Id });
        }
    }
}
