﻿using Dapper;
using System;
using System.Threading.Tasks;
using TinyUrl.Domain.Models;

namespace TinyUrl.Domain.Repositories
{
    public interface IUserRepository
    {
        Task<User> Get(Guid id);
        Task<User> Get(string userName, string password);
        Task Create(User user);
    }

    public class UserRepository : IUserRepository
    {
        private readonly DapperDbContext _context;

        public UserRepository(DapperDbContext dapperDbContext)
        {
            _context = dapperDbContext;
        }

        public async Task<User> Get(Guid id)
        {
            using var db = _context;
            var sqlQuery = "SELECT * FROM tiny_url.user WHERE id = @Id";
            return await db.Open.QueryFirstOrDefaultAsync<User>(sqlQuery, new { id });
        }

        public async Task<User> Get(string userName, string password)
        {
            using var db = _context;
            var sqlQuery = "SELECT * FROM tiny_url.user WHERE name = @Name AND password = @Password";
            return await db.Open.QueryFirstOrDefaultAsync<User>(sqlQuery, new { Name = userName, Password = password });
        }

        public async Task Create(User user)
        {
            using var db = _context;
            var sqlQuery = "INSERT INTO tiny_url.user (name, password, id) VALUES(@Name, @Password, @Id)";
            await db.Open.ExecuteAsync(sqlQuery, new { user.Id, user.Name, user.Password });
        }
    }
}
