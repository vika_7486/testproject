﻿using Microsoft.Extensions.Options;
using Npgsql;
using System;
using System.Data;

namespace TinyUrl.Domain
{
    public class DapperDbContext : IDisposable
    {
        private readonly IDbConnection _dbConnection;

        public DapperDbContext(IOptions<ConnectionString> connectionString)
        {
            var connectionStringValue = connectionString?.Value?.DefaultConnection ?? throw new ArgumentNullException(nameof(connectionString));
            _dbConnection = new NpgsqlConnection(connectionStringValue);
        }

        public IDbConnection Open
        {
            get
            {
                _dbConnection?.Open();
                return _dbConnection;
            }
        }

        public void Dispose()
        {
            _dbConnection?.Close();
        }
    }
}
