﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using TinyUrl.Domain.Models;
using TinyUrl.Domain.Repositories;

namespace TinyUrl.API.Managers
{
    public interface IUserManager
    {
        Task Create(string userName, string password);
        Task<ClaimsIdentity> GetIdentity(string userName, string password);
    }

    public class UserManager : IUserManager
    {
        private readonly IUserRepository _userRepository;

        public UserManager(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task Create(string userName, string password)
        {
            await _userRepository.Create(new User(userName, password));
        }

        public async Task<ClaimsIdentity> GetIdentity(string userName, string password)
        {
            var user = await _userRepository.Get(userName, password);
            if (user != null)
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, user.Name),
                    new Claim(ClaimTypes.NameIdentifier, user.Id.ToString())
                };
                var claimsIdentity = new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
                return claimsIdentity;
            }

            return null;
        }
    }
}
