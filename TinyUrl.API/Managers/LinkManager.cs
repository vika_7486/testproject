﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TinyUrl.API.Constants;
using TinyUrl.API.DTO;
using TinyUrl.API.Infrastructure;
using TinyUrl.Domain.Models;
using TinyUrl.Domain.Repositories;

namespace TinyUrl.API.Managers
{
    public interface ILinkManager
    {
        Task<Link> Search(string code);
        Task<List<LinkDto>> Search(Guid userId);
        Task<string> GetShortUrl(Guid userId, string originalUrl);
        Task UpdateCount(Link link);
    }

    public class LinkManager : ILinkManager
    {
        private readonly ILinkRepository _linkRepository;
        private readonly IMapper _mapper;

        public LinkManager(ILinkRepository linkRepository, IMapper mapper)
        {
            _linkRepository = linkRepository;
            _mapper = mapper;
        }

        public async Task<Link> Search(string code)
        {
            return await _linkRepository.Get(code);
        }

        public async Task<List<LinkDto>> Search(Guid userId)
        {
            var links = await _linkRepository.Get(userId);
            return links.Select(x => _mapper.Map<LinkDto>(x)).ToList();
        }

        public async Task<string> GetShortUrl(Guid userId, string originalUrl)
        {
            var link = await _linkRepository.Get(userId, originalUrl);

            if (link != null)
            {
                return link.UniqueCode;
            }

            var code = CodeGenerationHelper.Generate(TinyApiConstants.UniqueCodeLength);
            await _linkRepository.Create(new Link(originalUrl, code, userId));
            return code;
        }

        public async Task UpdateCount(Link link)
        {
            await _linkRepository.UpdateCount(link);
        }
    }
}
