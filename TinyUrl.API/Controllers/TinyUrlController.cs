﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using TinyUrl.API.Infrastructure.Extensions;
using TinyUrl.API.Managers;

namespace TinyUrl.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TinyUrlController : ControllerBase
    {
        private readonly ILogger<TinyUrlController> _logger;
        private readonly ILinkManager _linkManager;

        public TinyUrlController(ILogger<TinyUrlController> logger, ILinkManager linkManager)
        {
            _logger = logger;
            _linkManager = linkManager;
        }

        [Route("/{code}")]
        [HttpGet]
        public async Task<ActionResult> Get(string code)
        {
            var link = await _linkManager.Search(code);

            if (link == null)
            {
                return NotFound();
            }
            await _linkManager.UpdateCount(link);

            return Redirect(link.OriginalUrl);
        }

        [Authorize]
        [HttpGet("/getshorturl")]
        public async Task<ActionResult> GetShortUrl(string originalUrl)
        {
            var tinyUrl = await _linkManager.GetShortUrl(User.GetLoggedInUserId<Guid>(), originalUrl);
            return Ok(tinyUrl);
        }

        [Authorize]
        [HttpGet("/get_links_info_by_user")]
        public async Task<ActionResult> GetLinksInfo()
        {
            var model = await _linkManager.Search(User.GetLoggedInUserId<Guid>());
            return Ok(model);
        }
    }
}
