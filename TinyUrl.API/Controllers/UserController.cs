﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using TinyUrl.API.Infrastructure.AuthorizationHelpers;
using TinyUrl.API.Managers;
using TinyUrl.Domain.Models;

namespace TinyUrl.API.Controllers
{
    [ApiController]
    [Route("user")]
    public class UserController : ControllerBase
    {

        private readonly ILogger<UserController> _logger;
        private readonly IUserManager _userManager;

        public UserController(ILogger<UserController> logger, IUserManager userManager)
        {
            _logger = logger;
            _userManager = userManager;
        }

        [HttpPost("registration")]
        public async Task<ActionResult> Registration([FromBody] User user)
        {
            await _userManager.Create(user.Name, user.Password);
            return Ok();
        }

        [HttpPost("authorization")]
        public async Task<ActionResult> Authorization([FromBody] User user)
        {
            var identity = await _userManager.GetIdentity(user.Name, user.Password);
            if (identity == null)
            {
                return BadRequest(new { errorText = "Invalid username or password." });
            }

            var encodedJwt = JwtHelper.CreateToken(identity);
            var response = new
            {
                access_token = encodedJwt,
                username = identity.Name
            };

            return Ok(response);
        }

        [Authorize]
        [HttpGet("/getlogin")]
        public IActionResult GetLogin()
        {
            return Ok($"Ваш логин: {User.Identity.Name}");
        }
    }
}
