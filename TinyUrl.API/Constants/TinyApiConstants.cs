﻿namespace TinyUrl.API.Constants
{
    public static class TinyApiConstants
    {
        public const int UniqueCodeLength = 11;
    }
}
