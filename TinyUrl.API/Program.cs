using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using TinyUrl.API.Infrastructure.DatabaseBuilder;
using TinyUrl.Domain;

namespace TinyUrl.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();
            var connectionStringOptions = host.Services.GetRequiredService<IOptions<ConnectionString>>();
            DatabaseBuilder.CreateDb(connectionStringOptions.Value.DefaultConnection);
            host.Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
