﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using TinyUrl.API.DTO;
using TinyUrl.Domain.Models;

namespace TinyUrl.API.Profiles
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Link, LinkDto>()
                .ForMember(dest => dest.CreationDate, expression => expression.MapFrom(src => src.CreationDateTime.ToString("dd MMM yyyy, HH:mm")))
                .ForMember(dest => dest.OriginalUrl, expression => expression.MapFrom(src => src.OriginalUrl))
                .ForMember(dest => dest.ShortUrl, expression => expression.MapFrom<ShortUrlResolver>())
                .ForMember(dest => dest.ReferenceCount, expression => expression.MapFrom(src => src.ReferenceCount));
        }

        public class ShortUrlResolver : IValueResolver<Link, LinkDto, string>
        {
            private readonly IHttpContextAccessor _accessor;

            public ShortUrlResolver(IHttpContextAccessor accessor)
            {
                _accessor = accessor;
            }

            public string Resolve(Link source, LinkDto destination, string destMember, ResolutionContext context)
            {
                return $"{_accessor.HttpContext.Request.Scheme}://{_accessor.HttpContext.Request.Host}/{source.UniqueCode}";
            }
        }
    }
}