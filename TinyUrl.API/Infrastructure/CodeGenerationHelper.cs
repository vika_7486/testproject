﻿using System;
using System.Text;

namespace TinyUrl.API.Infrastructure
{
    public static class CodeGenerationHelper
    {
        private const string Chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-";

        public static string Generate(int length)
        {
            var charsLength = Chars.Length;
            var code = new StringBuilder();
            var random = new Random();
            do
            {
                code.Append(Chars[random.Next(0, charsLength)]);
            } while (code.Length < length);

            return code.ToString();
        }
    }
}
