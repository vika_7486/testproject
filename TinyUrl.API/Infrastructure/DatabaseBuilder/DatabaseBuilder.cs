﻿using Dapper;
using Npgsql;
using System.Data;

namespace TinyUrl.API.Infrastructure.DatabaseBuilder
{
    public static class DatabaseBuilder
    {
        public static void CreateDb(string connectionString)
        {
            using var dbConnection = new NpgsqlConnection(connectionString);
            {
                dbConnection.Open();
                using var transaction = dbConnection.BeginTransaction();
                {
                    try
                    {
                        CreateSchema(dbConnection);
                        CreateUserTable(dbConnection);
                        CreateLinkTable(dbConnection);
                        transaction.Commit();
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }
        }

        private static void CreateSchema(IDbConnection dbConnection)
        {
            const string sqlQuery = "CREATE SCHEMA IF NOT EXISTS tiny_url;";
            dbConnection.Execute(sqlQuery);
        }

        private static void CreateUserTable(IDbConnection dbConnection)
        {
            const string sqlQuery = "CREATE TABLE IF NOT EXISTS tiny_url.\"user\" " +
                                    "(id uuid NOT NULL," +
                                    "name character varying(100) COLLATE pg_catalog.\"default\" NOT NULL," +
                                    "password character varying(30) COLLATE pg_catalog.\"default\" NOT NULL," +
                                    "CONSTRAINT pk_user_id PRIMARY KEY (id)," +
                                    "CONSTRAINT unique_user_name UNIQUE (name))";

            dbConnection.Execute(sqlQuery);
        }

        private static void CreateLinkTable(IDbConnection dbConnection)
        {
            const string sqlQuery = "CREATE TABLE IF NOT EXISTS tiny_url.link " +
                                    "(id uuid NOT NULL," +
                                    "original_url character varying(200) COLLATE pg_catalog.\"default\" NOT NULL," +
                                    "unique_code character varying(20) COLLATE pg_catalog.\"default\" NOT NULL," +
                                    "creation_date_time timestamp without time zone NOT NULL DEFAULT LOCALTIMESTAMP," +
                                    "reference_count smallint NOT NULL DEFAULT 0," +
                                    " user_id uuid NOT NULL," +
                                    "CONSTRAINT pk_link_id PRIMARY KEY(id)," +
                                    "CONSTRAINT fk_link_user_id FOREIGN KEY(user_id) " +
                                    "REFERENCES tiny_url.\"user\"(id) MATCH SIMPLE " +
                                    "ON UPDATE NO ACTION " +
                                    "ON DELETE CASCADE " +
                                    "NOT VALID)";

            dbConnection.Execute(sqlQuery);
        }
    }
}
