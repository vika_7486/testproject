﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using System.Reflection;
using TinyUrl.API.Infrastructure.AuthorizationHelpers;
using TinyUrl.API.Managers;
using TinyUrl.API.Profiles;
using TinyUrl.Domain;
using TinyUrl.Domain.Repositories;

namespace TinyUrl.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = false;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true, // указывает, будет ли валидироваться издатель при валидации токена
                        ValidIssuer = AuthOptions.Issuer, // строка, представляющая издателя
                        ValidateAudience = true, // будет ли валидироваться потребитель токена
                        ValidAudience = AuthOptions.Audience, // установка потребителя токена
                        ValidateLifetime = true, // будет ли валидироваться время существования
                        IssuerSigningKey = AuthOptions.GetSymmetricSecurityKey(), // установка ключа безопасности
                        ValidateIssuerSigningKey = true // валидация ключа безопасности
                    };
                });

            services.Configure<ConnectionString>(Configuration.GetSection("ConnectionString"));

            services.AddScoped<DapperDbContext>();

            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<ILinkRepository, LinkRepository>();

            services.AddScoped<IUserManager, UserManager>();
            services.AddScoped<ILinkManager, LinkManager>();

            services.AddHttpContextAccessor();

            services.AddAutoMapper((provider, cfg) => cfg.AddProfile<MappingProfile>(), typeof(Startup).GetTypeInfo().Assembly);

            Dapper.DefaultTypeMap.MatchNamesWithUnderscores = true;

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
