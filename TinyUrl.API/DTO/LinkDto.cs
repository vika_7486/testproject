﻿namespace TinyUrl.API.DTO
{
    public class LinkDto
    {
        public string CreationDate { get; set; }
        public string OriginalUrl { get; set; }
        public string ShortUrl { get; set; }
        public int ReferenceCount { get; set; }
    }
}
